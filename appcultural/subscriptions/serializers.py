from rest_framework import serializers
from .models import Subscription, SubscriptionCalendar
from culturalspaces.serializers import CulturalSpaceSerializer
from culturalspaces.models import CulturalSpace
from rest_framework.settings import api_settings


class SubscriptionSerializer(serializers.ModelSerializer):
    #id = serializers.IntegerField(read_only=True)
    #email = serializers.EmailField(required=True)
    #cultural_space = CulturalSpaceSerializer()


    class Meta:
        model = Subscription
        fields = '__all__'

    def create(self, validated_data):
        cultural_space = CulturalSpace.objects.filter(id = validated_data['cultural_space'].id).first()
        subscription = Subscription(
            email = validated_data['email'],
            cultural_space = cultural_space
        )
        subscription.save()
        return subscription

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'email': instance.email,
            'cultural_space': CulturalSpaceSerializer(instance.cultural_space).data if instance.cultural_space != None else ''
         }


class SubscriptionCalendarSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = SubscriptionCalendar
        fields = '__all__'
