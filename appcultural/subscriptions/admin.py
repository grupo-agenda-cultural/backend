from django.contrib import admin
from .models import Subscription, SubscriptionCalendar

class SettingSubscription(admin.ModelAdmin):
    list_display = ('email','cultural_space')
class SettingSubscriptionCalendar(admin.ModelAdmin):
    list_display = ('email','email')
admin.site.register(Subscription, SettingSubscription)
admin.site.register(SubscriptionCalendar,SettingSubscriptionCalendar)
