from django.conf import settings
from django.core.mail import send_mail
from django.core.management.base import BaseCommand
from subscriptions.models import Subscription
from productions.models import Production
from subscriptions.models import Subscription
from subscriptions.correo import send_email_subscription

import datetime

class Command(BaseCommand):
    """
    Sends an email to the provided addresses.
    """

    help = 'Sends emails periodically'


    def handle(self, *args, **options):
        self.stdout.write(f"send_email from: {settings.EMAIL_HOST_USER}")
        start_week = datetime.datetime.today() - datetime.timedelta(days=datetime.datetime.today().weekday() % 7)
        end_week = start_week  + datetime.timedelta(days=6)
        subscriptions = Subscription.objects.all()
        for subscription in subscriptions:
            productions = Production.objects.filter(cultural_space = subscription.cultural_space.id, date_start__range=[start_week.strftime("%Y-%m-%d %H:%M:%S+00:00"), end_week.strftime("%Y-%m-%d %H:%M:%S+00:00")]).all()
            if productions:
                send_email_subscription(productions,subscription)
            else:
                'No hay producciones'
        self.stdout.write(self.style.SUCCESS('Successfully closed'))