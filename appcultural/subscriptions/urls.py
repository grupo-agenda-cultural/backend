from django.urls import path
from .views import AddSubscriptionView, AddSubscriptionCalendarView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('subscriptions/cultural_space/add/', AddSubscriptionView.as_view(), name="add_subscription"),
    path('subscriptions/calendar/add/', AddSubscriptionCalendarView.as_view(), name="add_subscription"),
]
