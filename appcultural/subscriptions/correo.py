from email.mime.text import MIMEText
import smtplib
from email.mime.multipart import MIMEMultipart
from django.conf import settings
from django.template.loader import render_to_string

def send_email_subscription(productions, subscription):
    try:
        mailServer = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
        print(mailServer.ehlo())
        mailServer.starttls()
        print(mailServer.ehlo())
        mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        print("Conectado")
        email_to = subscription.email
        print(email_to)

        # Create a message
        mensaje = MIMEMultipart()
        mensaje['From'] = settings.EMAIL_HOST_USER
        mensaje['To'] = email_to
        mensaje['Subject'] = "Novedades de la semana " + "["+ subscription.cultural_space.name + "]"
        
        print(productions,subscription)
        content = render_to_string('test_email.html', {'subscription': subscription, 'productions':productions})
        mensaje.attach(MIMEText(content,'html'))
        mailServer.sendmail(settings.EMAIL_HOST_USER, email_to, mensaje.as_string())
        print("Email enviado")
        mailServer.close()
   
    except Exception as e:
        print(e)

def send_email_subscription_calendar(productions, subscription):
    try:
        mailServer = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
        print(mailServer.ehlo())
        mailServer.starttls()
        print(mailServer.ehlo())
        mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        print("Conectado")
        email_to = subscription.email
        print(email_to)

        # Create a message
        mensaje = MIMEMultipart()
        mensaje['From'] = settings.EMAIL_HOST_USER
        mensaje['To'] = email_to
        mensaje['Subject'] = "Novedades de la semana"
        
        content = render_to_string('subscription_calendar.html', {'subscription': subscription, 'productions':productions})
        mensaje.attach(MIMEText(content,'html'))
        mailServer.sendmail(settings.EMAIL_HOST_USER, email_to, mensaje.as_string())
        print("Email enviado")
        mailServer.close()
   
    except Exception as e:
        print(e)