from django.db import models
class SubscriptionCalendar(models.Model):
    email = models.EmailField("Email",unique=True, max_length=255)
    class Meta:
            verbose_name = 'Subscription_Calendar'
            verbose_name_plural = 'Subscriptions_Calendar'
            
class Subscription(models.Model):
    email = models.EmailField("Email", max_length=255)
    cultural_space = models.ForeignKey("culturalspaces.CulturalSpace", on_delete=models.CASCADE, default="", blank=False, null=True, related_name='cultural_space_subscription')

    class Meta:
        verbose_name = 'Subscription_CulturalSpaces'
        verbose_name_plural = 'Subscriptions_CulturalSpace'
        unique_together = ['email', 'cultural_space']
 
