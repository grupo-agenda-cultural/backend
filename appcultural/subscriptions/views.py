from rest_framework import status, permissions, generics
from rest_framework.response import Response
from .serializers import SubscriptionSerializer, SubscriptionCalendarSerializer
class AddSubscriptionView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = SubscriptionSerializer

    def post(self, request):
        """
        Add subscription
        
        Add a new subscription
        """

        subscription_serializer = SubscriptionSerializer(data=request.data)
        if subscription_serializer.is_valid():
            subscription_serializer.save()
            return Response({'message: Suscripción exitosa'}, status=status.HTTP_201_CREATED)
        return Response(subscription_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddSubscriptionCalendarView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = SubscriptionSerializer

    def post(self, request):
        """
        Add subscription
        
        Add a new subscription
        """
        subscription_serializer = SubscriptionCalendarSerializer(data=request.data)
        if subscription_serializer.is_valid():
            subscription_serializer.save()
            return Response({'message: Suscripción exitosa'}, status=status.HTTP_201_CREATED)
        return Response(subscription_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
