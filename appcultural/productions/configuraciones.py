CHOICE_CATEGORIES = [
    (1,'Adultos'),
    (2,'Niños'),
    (3,'Público en general')
]


CHOICE_COSTS = [
    (1,'A la gorra'),
    (2,'Con precio'),
    (3,'Gratuito')
]

CHOICE_LANGUAGES = [    
    (1,'Artes Visuales'),
    (2,'Audiovisuales'),
    (3,'Danza'),
    (4,'Ferias'),
    (5,'Festival'),
    (6,'Letras'),
    (7,'Música'),
    (8,'Peña'),
    (9,'Teatro')
]

CHOICE_MODALITIES = [
    (1,'Presencial'),
    (2,'Virtual')
]

