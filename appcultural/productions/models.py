from django.db import models
from .configuraciones import CHOICE_CATEGORIES, CHOICE_COSTS, CHOICE_MODALITIES, CHOICE_LANGUAGES

def upload_path(instance, filename):
    return '/'.join(['productions',str(instance.name),filename])
class Production(models.Model):
    image = models.ImageField(upload_to=upload_path, null=True, blank=False)    
    name = models.CharField(max_length=255, blank=True, null=False)
    organizer_person = models.CharField(max_length=255, blank=False)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField(null=True, blank=True)
    description = models.TextField()
    modality = models.IntegerField(blank=False, choices=CHOICE_MODALITIES)
    language = models.IntegerField(blank=False, choices=CHOICE_LANGUAGES)
    category = models.IntegerField(blank=False, choices=CHOICE_CATEGORIES)
    cost = models.IntegerField(blank=False, choices=CHOICE_COSTS)
    price = models.DecimalField(decimal_places=2, max_digits=10, default=0, blank=True)
    cultural_space = models.ForeignKey("culturalspaces.CulturalSpace", on_delete=models.CASCADE, default="", blank=False, null=True, related_name='cultural_space_production')
    class Meta:
        verbose_name = 'Production'
        verbose_name_plural = 'Productions'
        ordering = ['-date_start','name']

    def __str__(self):
        return f'{self.id} {self.name}'

    def __unicode__(self):
        return self.image
