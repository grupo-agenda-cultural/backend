from functools import partial
from itertools import product
from math import prod
import os
from culturalspaces.serializers import CulturalSpaceSerializer
from culturalspaces.models import CulturalSpace
from rest_framework.response import Response
from rest_framework import status, permissions
from rest_framework.views import APIView
from .serializers import ProductionSerializer
from .models import Production
from rest_framework import generics
from users.models import Coordinador
import datetime
from django.forms import model_to_dict
from django.conf import settings

class AddProductionView(generics.CreateAPIView):
    serializer_class = ProductionSerializer
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        """
        Add a new production

        Add production
        """
        if request.user.is_authenticated:
            coordinador = Coordinador.objects.filter(user_id=request.user.id).first()
            cultural_space = CulturalSpace.objects.filter(id = coordinador.cultural_space.id).first()
            if cultural_space:
                production_serializer = ProductionSerializer(data=request.data, context = {"request": cultural_space})
                if production_serializer.is_valid():
                    production_serializer.save()
                    return Response(production_serializer.data, status=status.HTTP_201_CREATED)
                return Response(production_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response({'message: El espacio cultural no es válido'}, status=status.HTTP_201_CREATED)
        return Response('message: El usuario no está autenticado', status=status.HTTP_400_BAD_REQUEST)

class ListProductionView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = ProductionSerializer

    def get(self, request):
        """
        Get all productions 

        Return a list of all productions 
        """
        productions = Production.objects.order_by('name').all()
        production_serializer = ProductionSerializer(productions, many=True)
        return Response(production_serializer.data, status=status.HTTP_200_OK)
class ListProductionsView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ProductionSerializer

    def get(self, request):
        """
        Get all productions by my cultural space

        Return a list of all productions by my cultural space
        """
        if request.user.is_authenticated:
            coordinador = Coordinador.objects.filter(user_id=request.user.id).first()
            if coordinador.cultural_space != None:
                productions = Production.objects.filter(cultural_space_id = coordinador.cultural_space.id).order_by('name').all()
                production_serializer = ProductionSerializer(productions, many=True, context={'request': request})
                return Response(production_serializer.data, status = status.HTTP_200_OK)
        return Response('message: El usuario no está autenticado', status = status.HTTP_400_BAD_REQUEST)

class GetProductionView(generics.RetrieveAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = ProductionSerializer

    def get(self, request, pk=None):
        """
        Get production by id

        Get a production by id
        """
        production = Production.objects.filter(id=pk).first()
        if production:
            production_serializer = ProductionSerializer(production)
            return Response(production_serializer.data, status=status.HTTP_200_OK)
        return Response({'message: No se ha encontrado una producción con estos datos'}, status=status.HTTP_400_BAD_REQUEST)
class UpdateProductionView(generics.UpdateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ProductionSerializer

    def get_queryset(self,pk):
        return self.get_serializer().Meta.model.objects.filter(id=pk).first()

    def patch(self, request, pk=None):
        """
        Update (patch) a production by id

        Update production
        """
        if request.user.is_authenticated:
            production = Production.objects.filter(id=pk).first()
            if production:
                production_serializer = ProductionSerializer(production, data=request.data)
                if production_serializer.is_valid():
                    production_serializer.save()
                    return Response(production_serializer.data, status=status.HTTP_200_OK)
                return Response('message: No existe una producción con estos datos',status=status.HTTP_400_BAD_REQUEST)
            return Response('message: No existe una producción con estos datos',status=status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status=status.HTTP_400_BAD_REQUEST)

        # production = self.get_queryset(pk)
        # if production:
        #     production_serializer = self.serializer_class(production)  
        #     return Response(production_serializer.data, status=status.HTTP_201_CREATED)
        # return Response(status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk=None):
        """
        Update (replace) a production by id

        Update production
        """
        if self.get_queryset(pk):
            production_serializer = self.serializer_class(self.get_queryset(pk), data = request.data)
            if production_serializer.is_valid():
                production_serializer.save()
                return Response(production_serializer.data, status=status.HTTP_201_CREATED)
            return Response(production_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DeleteProductionView(generics.DestroyAPIView):
    serializer_class = ProductionSerializer
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, pk=None):
        """
        Delete a production by id
        
        Delete production
        """
        if request.user.is_authenticated:
            production = Production.objects.filter(id=pk).first()
            if production:
                production.delete()
                return Response({'message: Producción eliminada correctamente'}, status=status.HTTP_200_OK)
            return Response({'message: No se ha encontrado una producción con estos datos'}, status=status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status=status.HTTP_400_BAD_REQUEST)


class GetProductionsCalendarView(APIView):
    permission_classes = (permissions.AllowAny,)
    
    def get(self, request):        
        """
        Get all culturalspaces, productions and coordinador phone

        Get all productions
        """
        start_calendar = datetime.datetime.today()
        end_calendar = start_calendar  + datetime.timedelta(days=90)
        result = []
        coordinadores = Coordinador.objects.select_related('cultural_space').all()
        for c in coordinadores:
            if c.cultural_space != None:
                user = model_to_dict(c.user, fields=['name','phone'])
                productions = Production.objects.filter(cultural_space_id = c.cultural_space.id, date_end__range=[start_calendar.strftime("%Y-%m-%d %H:%M:%S+00:00"), end_calendar.strftime("%Y-%m-%d %H:%M:%S+00:00")]).all().order_by('date_start')

                for p in productions:
                    production = model_to_dict(p, fields=['id','name','image','organizer_person','description', 'date_start','date_end', 'category','language', 'modality','cost','price','cultural_space'])
                    production['image'] = production['image'].url
                    cultural_space =  model_to_dict(p.cultural_space, fields=['id','image','name', 'address', 'email','phone','localidad','partido', 'latitude','longitude','instagram','facebook'])
                    cultural_space['image'] = cultural_space['image'].url
                    result_response = {"coordinador":user, "cultural_space": cultural_space, "production": production }
                   
                    result.append(result_response)
             
        return Response(result, status=status.HTTP_200_OK)

class GetProductionEventView(APIView):
    permission_classes = (permissions.AllowAny,)
   
    def get(self, request, pk):
        """
        Get all productions and culturalspaces

        Get all productions
        """
        result=[]
        productions = Production.objects.filter(id=pk).all()
        for p in productions:
            production = model_to_dict(p, fields=['id','name','image','organizer_person','description', 'date_start','date_end', 'category','language', 'modality','cost','price','cultural_space'])
            production['image'] = production['image'].url
            cultural_space =  model_to_dict(p.cultural_space, fields=['id','image','name', 'address', 'email','phone','localidad','partido', 'latitude','longitude','instagram', 'facebook'])
            cultural_space['image'] = cultural_space['image'].url

            coordinador = Coordinador.objects.filter(cultural_space_id = p.cultural_space).all()
            for c in coordinador:
                if c.cultural_space != None:
                    user = model_to_dict(c.user, fields=['name','phone'])
     
                    result_response = {"coordinador":user, "cultural_space": cultural_space, "production": production }
           
                    result.append(result_response)

        return Response(result, status=status.HTTP_200_OK)




class GetProductionsByCulturalSpaceView(APIView):
   permission_classes = (permissions.AllowAny,)
    
   def get(self, request, pk=None):        
        """
        Get all culturalspaces, productions and coordinador phone

        Get all productions
        """
        start_calendar = datetime.datetime.today()
        end_calendar = start_calendar  + datetime.timedelta(days=90)
        result = []
      
        coordinadores = Coordinador.objects.filter(cultural_space = pk).all()
        for c in coordinadores:
             if c.cultural_space != None:
                 user = model_to_dict(c.user, fields=['name','phone'])
                 productions = Production.objects.filter(cultural_space_id = pk, date_end__range=[start_calendar.strftime("%Y-%m-%d %H:%M:%S+00:00"), end_calendar.strftime("%Y-%m-%d %H:%M:%S+00:00")]).all().order_by('date_start')

                 for p in productions:

                    #         user = model_to_dict(c.user, fields=['name','phone'])

                    production = model_to_dict(p, fields=['id','name','image','organizer_person','description', 'date_start','date_end', 'category','language', 'modality','cost','price','cultural_space'])
                    production['image'] = production['image'].url
                    cultural_space =  model_to_dict(p.cultural_space, fields=['id','image','name', 'address', 'email','phone','localidad','partido', 'latitude','longitude', 'instagram','facebook'])
                    cultural_space['image'] = cultural_space['image'].url
                    result_response = {"coordinador":user, "cultural_space": cultural_space, "production": production }
                    result.append(result_response)
             
        return Response(result, status=status.HTTP_200_OK)
