from django.urls import path, re_path
from .views import AddProductionView, ListProductionView, ListProductionsView, DeleteProductionView, GetProductionView, UpdateProductionView, GetProductionsCalendarView,GetProductionEventView, GetProductionsByCulturalSpaceView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('productions/add/', AddProductionView.as_view(), name='add_production_view'),
    path('productions/', ListProductionView.as_view(), name='list_production_view'),
    path('productions/my-cultural-space/', ListProductionsView.as_view(), name='list_production_view'),

    path('productions/<int:pk>/', GetProductionView.as_view(), name='_get_production_view'),
    path('productions/update/<int:pk>/', UpdateProductionView.as_view(), name='update_production_view'),
    path('productions/delete/<int:pk>/', DeleteProductionView.as_view(), name='_delete_production_view'),
    path('productions/calendar/', GetProductionsCalendarView.as_view(), name='get_course_view'),
    path('productions/calendar/<int:pk>/', GetProductionEventView.as_view(), name="productions_cultural_space"),
    path('productions/calendar/culturalspaces/<int:pk>/', GetProductionsByCulturalSpaceView.as_view(), name="productions_calendar_cultural_space")
]

#urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) 
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


