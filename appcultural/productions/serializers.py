from email.mime import image
from django.conf import settings
from culturalspaces.models import CulturalSpace
from rest_framework import serializers
from .models import Production
from rest_framework.settings import api_settings
from django.urls import reverse
from django.http import HttpRequest
from rest_framework.request import Request
class ProductionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    image = serializers.ImageField(use_url=True, allow_null=True, required=False)
    name = serializers.CharField(max_length=255)
    organizer_person = serializers.CharField(max_length=255)
    date_start = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT,input_formats=api_settings.DATE_INPUT_FORMATS)
    date_end = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT,input_formats=api_settings.DATE_INPUT_FORMATS)
    description = serializers.CharField()
    modality = serializers.CharField(max_length=255)
    language = serializers.CharField(max_length=255)
    category = serializers.CharField(max_length=255)
    cost = serializers.CharField(max_length=255)
    price = serializers.DecimalField(decimal_places=2, max_digits=10, default=0)
    cultural_space = serializers.IntegerField(read_only=True)

    class Meta:
        model = Production
        fields = 'id', 'name','image','organizer_person', 'description','date_start','date_end','language','category','cost','modality','price','cultural_space'
    
    def create(self, validated_data):
        cultural_space = CulturalSpace.objects.filter(id = self.context['request'].id).first()
        production = Production(
            image = validated_data['image'],
            name = validated_data['name'],
            organizer_person = validated_data['organizer_person'],
            date_start = validated_data['date_start'],
            date_end = validated_data['date_end'],
            description = validated_data['description'],
            language = validated_data['language'],
            modality = validated_data['modality'],                
            category = validated_data['category'],            
            cost = validated_data['cost'],
            price = validated_data['price'],
            cultural_space = cultural_space
        )
        production.save()
        return production

    def update(self, instance, validated_data): 
        instance.image = validated_data.get('image', instance.image)
        instance.name = validated_data.get('name', instance.name)
        instance.organizer_person = validated_data.get('organizer_person', instance.organizer_person)
        instance.date_start = validated_data.get('date_start', instance.date_start)
        instance.date_end = validated_data.get('date_end', instance.date_end)
        instance.description = validated_data.get('description', instance.description)
        instance.language = validated_data.get('language', instance.language)
        instance.modality = validated_data.get('modality', instance.modality)
        instance.category = validated_data.get('category', instance.category)
        instance.cost = validated_data.get('cost', instance.cost)
        instance.price = validated_data.get('price', instance.price)
        instance.cultural_space = validated_data.get('cultural_space', instance.cultural_space)
        instance.save()
        return instance
    
    def to_representation(self,instance):
       from culturalspaces.serializers import CulturalSpaceSerializer
       return {
             'id': instance.id,
             'name': instance.name,
             'image': instance.image.url,
             'organizer_person': instance.organizer_person,
             'description': instance.description,
             'date_start': instance.date_start,
             'date_end': instance.date_end,
             'language': instance.language,
             'category': instance.category,
             'modality': instance.modality,
             'cost': instance.cost,
             'price': instance.price,
            'cultural_space': CulturalSpaceSerializer(instance.cultural_space).data,
        }  