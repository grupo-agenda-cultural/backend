from django.test import SimpleTestCase
from users.views import RegisterView, LoginView, LogoutView, ListCoordinadoresView
from django.urls import reverse, resolve

class TestUrlsTestCase(SimpleTestCase):

    def test_register_url(self):
        url = reverse('register_user')
        self.assertEquals(resolve(url).func.view_class, RegisterView)

    def test_login_url(self):
        url = reverse('login')
        self.assertEquals(resolve(url).func.view_class, LoginView) 
    
    def test_logout_url(self):
        url = reverse('logout')
        self.assertEquals(resolve(url).func.view_class, LogoutView)
    
    def test_list_coordinadores_url(self):
        url = reverse('list_coordinadores')
        self.assertEquals(resolve(url).func.view_class, ListCoordinadoresView)