from django.test import TestCase, Client
from users.views import RegisterView, LoginView, LogoutView, ListCoordinadoresView
from django.contrib.auth.models import Group
from django.urls import reverse, resolve
from users.models import User
import json

class TestViewsTestCase(TestCase):

    def setUp(self):
        self.group, created = Group.objects.get_or_create(name='Coordinador')
        self.name = 'Pablo'
        self.last_name = 'Gonzalez'
        self.phone = '1134779188'
        self.email = 'test.pablogonzalez@gmail.com'
        self.password = 'pablo1234'
        self.is_active = False

    def test_register_user(self):
        response = self.client.post(reverse('register_user'), data = {
            'name': self.name,
            'last_name': self.last_name,
            'phone': self.phone,
            'email': self.email,
            'password': self.password,
            'is_active': self.is_active
        })
        self.assertEquals(response.status_code, 201)


    # def test_confirm_account(self):
    #    response = self.client.put(reverse('confirm_account_user'), kwargs={'pk': 1}) 
    #    print(response.status_code)

