from django.test import TestCase
from users.models import User

class UserModelTestCase(TestCase):
    
    def setUp(self):
        self.user = User.objects._create_user(
            email = "admin@gmail.com",
            name = "Jorge",
            last_name = "Gutierrez",
            phone = "1134567788",
            password = "password",
            is_superuser = False,
            is_staff = False
        )

    def test_user_creation(self):
        self.assertEqual(self.user.is_active,False)
        self.assertEqual(self.user.is_staff, False)
        self.assertEqual(self.user.is_superuser,False)