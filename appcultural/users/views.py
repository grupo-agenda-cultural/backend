from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import status, permissions, generics
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from django.contrib.auth.models import Group
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated
from .models import User, Coordinador, Administrador
from .serializers import UserModelSerializer,CustomUserModelSerializer, MyTokenObtainPairSerializer, GroupSerializer, CoordinadorSerializer, AdministradorSerializer
from rest_framework import generics
from django.core.mail import send_mail
from users.correo import send_email, send_email_confirmation
from culturalspaces.serializers import CulturalSpaceSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import authenticate
class ObtainTokenPairView(TokenObtainPairView):
    pass

class RegisterView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserModelSerializer

    def post(self, request):
        """
        Register a new user

        Register a new user (Coordinador) and send confirmation email
        """
        user_serializer = UserModelSerializer(data=request.data)
        if user_serializer.is_valid():
            user_serializer.save()

            # Send register email
            send_email(user_serializer)
            return Response({'message: Usuario creado correctamente'}, status = status.HTTP_201_CREATED)
        return Response(user_serializer.errors, status = status.HTTP_400_BAD_REQUEST)


# class ListUsersView(APIView):
#     permission_classes = [permissions.IsAuthenticated]

#     def get(self, request):
#         """
#         Get all users

#         Return a list of all users: Administradores and Coordinadores
#         """
#         users = User.objects.all()
#         users_serializer = UserModelSerializer(users, many=True)
#         return Response(users_serializer.data, status=status.HTTP_200_OK)


class ListCoordinadoresView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = CoordinadorSerializer

    def get(self, request):
        """
        Get all Coordinadores

        Return a list of Coordinadores
        """
        if request.user.is_authenticated:
            users = Coordinador.objects.all()
            if users:
                users_serializer = CoordinadorSerializer(users, many=True)
                return Response(users_serializer.data, status = status.HTTP_200_OK)
            return Response('message: No se han encontrado coordinadores', status = status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status = status.HTTP_400_BAD_REQUEST)


class UpdateUserView(generics.UpdateAPIView):
    serializer_class = CustomUserModelSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self, pk):
        return self.get_serializer().Meta.model.objects.filter(id=pk).first()

    def patch(self, request, pk=None):
        if request.user.is_authenticated:
            user = User.objects.filter(id=pk).first()
            if user:
                user_serializer = CustomUserModelSerializer(user, data=request.data, partial=True)
                if user_serializer.is_valid():
                    user_serializer.save()
                    return Response(user_serializer.data, status = status.HTTP_200_OK)
            return Response('message: No existe un usuario con estos datos',status = status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status = status.HTTP_400_BAD_REQUEST)


    def put(self, request, pk=None):
        if self.get_queryset(pk):
            user_serializer = self.serializer_class(
                self.get_queryset(pk), data=request.data)
            if user_serializer.is_valid():
                user_serializer.save()
                # if (user_serializer.data['is_active']):
                # send_email_confirmation(user_serializer)

                return Response(user_serializer.data, status=status.HTTP_200_OK)
            return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UpdateConfirmAccountUserView(generics.UpdateAPIView):
    serializer_class = UserModelSerializer
    permission_classes = (permissions.AllowAny,)
    
    def get_queryset(self, pk):
        return self.get_serializer().Meta.model.objects.filter(id=pk).first()

    def patch(self, request, pk=None):
        if request.user.is_authenticated:
            user = User.objects.filter(id=pk).first()
            if user:
                user_serializer = CustomUserModelSerializer(user, data=request.data, partial=True)
                if user_serializer.is_valid():
                    user_serializer.save()
                    send_email_confirmation(user_serializer)
                    return Response(user_serializer.data, status=status.HTTP_200_OK)
                #return Response('message: No existe una producción con estos datos',status=status.HTTP_400_BAD_REQUEST)
            return Response('message: No existe un usuario con estos datos',status=status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, pk=None):
        if self.get_queryset(pk):
            user_serializer = self.serializer_class(
                self.get_queryset(pk), data=request.data)
            if user_serializer.is_valid():
                user_serializer.save()
                send_email_confirmation(user_serializer)
                return Response(user_serializer.data, status=status.HTTP_200_OK)
            return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UpdateCoordinadorView(generics.UpdateAPIView):
    serializer_class = CoordinadorSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self, pk):
        return self.get_serializer().Meta.model.objects.filter(id=pk).first()

    def patch(self, request, pk=None):
        user = self.get_queryset(pk)
        if user:
            user_serializer = self.serializer_class(user)
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk=None):
        if self.get_queryset(pk):
            user_serializer = self.serializer_class(
                self.get_queryset(pk), data=request.data)
            if user_serializer.is_valid():
                user_serializer.save()
                return Response(user_serializer.data, status=status.HTTP_200_OK)
            return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
class LoginView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        """
        Authenticate with email and password

        Login/Authenticate        
        """
        email = request.data.get('email','')
        password = request.data.get('password','')

        user = authenticate(email=email, password=password)
        if user:
            login_serializer = self.serializer_class(data=request.data)
            if login_serializer.is_valid():
                user_serializer = CustomUserModelSerializer(user)
                return Response({
                    'access_token': login_serializer.validated_data.get('access'),
                    'refresh_token': login_serializer.validated_data.get('refresh'),
                    'user': user_serializer.data,
                    'message':'Inicio de sesión exitoso'
                },status=status.HTTP_200_OK)
            return Response({'error':'Usuario o contraseña incorrectos'}, status = status.HTTP_400_BAD_REQUEST)
        return Response({'error':'Usuario o contraseña incorrectos'}, status = status.HTTP_400_BAD_REQUEST)


class LogoutView(APIView):
    def post(self, request, *args, **kwargs):
        """
        Logout session user

        Logout      
        """
        user = User.objects.filter(id=request.data.get('user',0))
        if user.exists():
            RefreshToken.for_user(user.first())
            return Response({'message':'Sesión cerrada correctamente'}, status = status.HTTP_200_OK)
        return Response({'error':'No existe este usuario'}, status = status.HTTP_400_BAD_REQUEST)

class GetUserCoordinadorView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = CoordinadorSerializer

    def get(self, request):
        if request.user.is_authenticated:
            user = request.user
            coordinador = Coordinador.objects.filter(user_id = user.id).first()
            if coordinador:
                coordinador_serializer = CoordinadorSerializer(coordinador)
                return Response(coordinador_serializer.data, status=status.HTTP_200_OK)
            else:
                administrador = Administrador.objects.filter(user_id = user.id).first()
                administrador_serializer = AdministradorSerializer(administrador)
                return Response(administrador_serializer.data, status=status.HTTP_200_OK)
        return Response('message: El usuario no está autenticado', status = status.HTTP_400_BAD_REQUEST)
