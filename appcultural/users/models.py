
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin, Permission, Group
from django.db import models
from django.contrib.contenttypes.models import ContentType
from rest_framework_simplejwt.tokens import RefreshToken
from culturalspaces.models import CulturalSpace

#super_administrador_group, created = Group.objects.get_or_create(name='SuperAdministrador')
#administrador_group, created = Group.objects.get_or_create(name='Administrador')
#coordinador_group, created = Group.objects.get_or_create(name='Coordinador')
class UserManager(BaseUserManager):
    def _create_user(self, name, last_name, email, phone, password, is_staff, is_superuser, **extra_fields):
        user = self.model(
            name=name,
            last_name=last_name,
            email=email,
            phone=phone,
            is_staff=is_staff,
            is_superuser=is_superuser,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_user(self, name, last_name, email, phone, password=None, **extra_fields):
        return self._create_user(name, last_name, email, phone, password, False, False, **extra_fields)

    def create_superuser(self, name, last_name, email, phone, password=None, **extra_fields):
        return self._create_user(name, last_name, email, phone, password, True, True, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    username = None
    name = models.CharField("Name", max_length=255)
    last_name = models.CharField("Last name", max_length=255)
    email = models.EmailField("Email", max_length=255, unique=True)
    phone = models.CharField("Phone",max_length=20)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    objects = UserManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
        ordering = ['-is_active', '-name']

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'last_name', 'phone']

    def __str__(self):
        return f'{self.name} {self.last_name}'


class Administrador(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Administrador'
        verbose_name_plural = 'Administradores'
    
    def __str__(self):
        return f'{self.user.name} {self.user.last_name}'


class Coordinador(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cultural_space = models.OneToOneField(CulturalSpace, on_delete=models.CASCADE, related_name='cultural_space', default="", null=True, blank=True)
    class Meta:
        verbose_name = 'Coordinador'
        verbose_name_plural = 'Coordinadores'
        ordering = ['-user']

    def __str__(self):
        return f'{self.user.name} {self.user.last_name}'