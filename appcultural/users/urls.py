from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from .views import  RegisterView, LoginView, LogoutView, ListCoordinadoresView, UpdateUserView, UpdateConfirmAccountUserView, UpdateCoordinadorView, GetUserCoordinadorView

urlpatterns = [
    path('auth/', include('djoser.urls')),
    path('auth/register/', RegisterView.as_view(), name="register_user"),
    path('auth/login/', LoginView.as_view(), name='login'),
    path('auth/logout/', LogoutView.as_view(), name='logout'),
    path('auth/login/me/',GetUserCoordinadorView.as_view(), name="login_me"),
    path('token/', TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path('token/refresh/', TokenRefreshView.as_view(), name="token_refresh"),

    path('users/update/<int:pk>/', UpdateUserView.as_view(), name='user_update_view'),
    path('users/confirm/<int:pk>/', UpdateConfirmAccountUserView.as_view(), name='confirm_account_user'),
    path('users/coordinadores/', ListCoordinadoresView.as_view(), name='list_coordinadores'),
    path('users/coordinadores/update/<int:pk>/', UpdateCoordinadorView.as_view(), name='coordinador_update_views_api'),
]
