from django.contrib import admin
from .models import User, Coordinador, Administrador
class SettingUser(admin.ModelAdmin):
    list_display = ('name','last_name','email','phone','is_active')

class SettingCoordinador(admin.ModelAdmin):
    list_display = ('user','cultural_space')



admin.site.register(User, SettingUser)
admin.site.register(Coordinador, SettingCoordinador)
admin.site.register(Administrador)
