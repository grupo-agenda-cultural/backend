from asyncore import read
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from .models import Administrador, User, Coordinador
from django.contrib.auth.models import Group
#from django.contrib.auth import authenticate, login, logout
from culturalspaces.serializers import CulturalSpaceSerializer
from rest_framework_simplejwt.tokens import RefreshToken, TokenError

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    pass
class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name')


class CustomUserModelSerializer(serializers.ModelSerializer):
    groups = serializers.SerializerMethodField('get_groups')

    class Meta:
        model = User
        fields = ('id', 'email', 'name', 'last_name','phone','is_active', 'groups')

    def update(self, instance, validated_data): 
        instance.name = validated_data.get('name', instance.name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.email = validated_data.get('email', instance.email)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.save()
        return instance

    def get_groups(user, request):
        groups = Group.objects.filter(user=request)
        group_tuple = [{"id": group.id, "name": group.name} for group in groups]
        return group_tuple

    def to_representation(self,instance):
         return {
            'id': instance.id,
            'email': instance.email,
            'name': instance.name,
            'last_name': instance.last_name,
            'phone': instance.phone,
            'is_active': instance.is_active,
            'groups': self.get_groups(instance),
        }  
class UserModelSerializer(serializers.ModelSerializer):
    groups = serializers.SerializerMethodField('get_groups')

    class Meta:
        model = User
        fields = ('id', 'email', 'name', 'last_name', 'password', 'phone', 'is_active','groups')
        extra_kwargs = ({'password': {'write_only': False}})

    def get_groups(user, request):
        groups = Group.objects.filter(user=request)
        group_tuple = [{group.id,  group.name} for group in groups]
        return group_tuple

    def create(self, validated_data, *args, **kwargs):
        password = validated_data.pop('password', None)
        groups_data = validated_data.pop('groups', [])
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        if groups_data is not None:
            coordinador = Coordinador(user=instance)
            coordinador.save()
            instance.groups.add(Group.objects.get(name='Coordinador'))
        return instance

    def to_representation(self,instance):
         return {
            'id': instance.id,
            'email': instance.email,
            'name': instance.name,
            'last_name': instance.last_name,
            'phone': instance.phone,
            'password': instance.password,
            'is_active': instance.is_active,
            'groups': self.get_groups(instance),
        }     

class AdministradorSerializer(serializers.ModelSerializer):
    user = CustomUserModelSerializer(read_only=True)
    class Meta:
        model = '_all_'

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'user': CustomUserModelSerializer(instance.user).data,
         }

class CoordinadorSerializer(serializers.ModelSerializer):
    user = CustomUserModelSerializer(read_only=True)
    cultural_space = serializers.IntegerField(read_only=True)

    class Meta:
        model = Coordinador
        fields = 'id','user','cultural_space'
        
    def update(self, instance, validated_data): 
        instance.user = validated_data.get('user', instance.user)
        instance.cultural_space = validated_data.get('cultural_space', instance.cultural_space)
        instance.save()
        return instance
    
    
    def to_representation(self, instance):
        return {
            'id': instance.id,
            'user': CustomUserModelSerializer(instance.user).data,
            'cultural_space': CulturalSpaceSerializer(instance.cultural_space).data if instance.cultural_space != None else ''
         }
