from email.mime.text import MIMEText
import smtplib
from email.mime.multipart import MIMEMultipart
from django.conf import settings
from django.template.loader import render_to_string

def send_email(user_serializer):
    try:
        mailServer = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
        print(mailServer.ehlo())
        mailServer.starttls()
        print(mailServer.ehlo())
        mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        print("Conectado")
        email_to = user_serializer.data['email']
        print(email_to)

        # Create a message
        mensaje = MIMEMultipart()
        mensaje['From'] = settings.EMAIL_HOST_USER
        mensaje['To'] = email_to
        mensaje['Subject'] = "Bienvenid@ a A.C.C.A."
        
        user = user_serializer.data['name'] + " " + user_serializer.data['last_name']
        email = user_serializer.data['email']
        content = render_to_string('send_email.html', {'user': user})
        mensaje.attach(MIMEText(content,'html'))
        mailServer.sendmail(settings.EMAIL_HOST_USER, email_to, mensaje.as_string())
        print("Email enviado")
        mailServer.close()

    except Exception as e:
        print(e)

def send_email_confirmation(user_serializer):
    try:
        mailServer = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
        print(mailServer.ehlo())
        mailServer.starttls()
        print(mailServer.ehlo())
        mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        print("Conectado")
        email_to = user_serializer.data['email']
        print(email_to)

        # Create a message
        mensaje = MIMEMultipart()
        mensaje['From'] = settings.EMAIL_HOST_USER
        mensaje['To'] = email_to
        mensaje['Subject'] = "Confirmación de cuenta"
        
        user = user_serializer.data['name'] + " " + user_serializer.data['last_name']
        email = user_serializer.data['email']
        content = render_to_string('send_email_confirmation.html', {'user': user, 'email': email})
        mensaje.attach(MIMEText(content,'html'))
        mailServer.sendmail(settings.EMAIL_HOST_USER, email_to, mensaje.as_string())
        print("Email enviado")
        mailServer.close()
   
    except Exception as e:
        print(e)



