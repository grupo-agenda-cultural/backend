# ACCA - Backend

# Install
## Create virtual environment 

```
$ virtualenv myvenv -p python3
```
### Execute virtual environment 
```
$ source myvenv/bin/activate
```

Clone repository
```
$ git clone https://gitlab.com/grupo-agenda-cultural/backend
```

Run project
```
$ python manage.py runserver
```
# Technologies and tools
 - 
## Crontab
Run this command to add all defined jobs from CRONJOBS.  
Show current active jobs of this project:

```
python manage.py crontab add
```

Show current active jobs of this project:
```
python manage.py crontab show
```

Removing all defined jobs
```
python manage.py crontab remove
```
