
CHOICE_CATEGORIES = [
    (1,'Adultos'),
    (2,'Niños'),
    (3,'Público en general')
]

CHOICE_MODALITIES = [
    (1,'Presencial'),
    (2,'Virtual')
]

CHOICE_LANGUAGES = [    
    (1,'Audiovisuales'),
    (2,'Artes Visuales'),
    (3,'Artesanías'),
    (4,'Comunicación'),
    (5,'Danza'),
    (6,'Letras'),
    (7,'Música'),
    (8,'Teatro')
]

CHOICE_COSTS = [
    (1,'A la gorra'),
    (2,'Con precio'),
    (3,'Gratuito')
]

CHOICE_DURATIONS = [
    (1,'Anual'),
    (2,'Mensual'),
    (3,'Semestral')
]

CHOICE_FORMATIONS = [
    (1,'Congreso'),   
    (2,'Curso'),
    (3,'Diplomatura'),
    (4,'Encuentro'),
    (5,'Jornada'),
    (6,'Seminario'),
    (7,'Workshop')
]