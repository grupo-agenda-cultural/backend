from rest_framework import serializers
from .models import Course
from culturalspaces.models import CulturalSpace
from rest_framework.settings import api_settings

class CourseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    image = serializers.ImageField(use_url=True, allow_null=True, required=False)
    name = serializers.CharField(max_length=255)
    teacher = serializers.CharField(max_length=255)
    description = serializers.CharField()
    content = serializers.CharField()
    date = serializers.CharField(max_length=255)
    hour = serializers.CharField(max_length=255)
    inscription_start = serializers.DateField(format=api_settings.DATE_FORMAT,input_formats=api_settings.DATE_INPUT_FORMATS)
    inscription_end = serializers.DateField(format=api_settings.DATE_FORMAT,input_formats=api_settings.DATE_INPUT_FORMATS)
    language = serializers.CharField(max_length=255)
    formation = serializers.CharField(max_length=255)
    category = serializers.CharField(max_length=255)
    duration = serializers.CharField(max_length=255)
    modality = serializers.CharField(max_length=255)
    requirements = serializers.CharField(allow_blank=True)
    cost = serializers.CharField(max_length=255)
    price = serializers.DecimalField(decimal_places=2, max_digits=10, default=0)
    observations = serializers.CharField(allow_blank=True)
    cultural_space = serializers.IntegerField(read_only=True)


    class Meta:
        model = Course
        fields= '__all__'
        # extra_kwargs = ({'observations': {'allow_blank': True,'required': False}})    

    def create(self, validated_data):
        cultural_space = CulturalSpace.objects.filter(id = self.context['request'].id).first()
        course = Course(
            image = validated_data['image'],
            name = validated_data['name'],
            teacher = validated_data['teacher'],
            description = validated_data['description'],
            date = validated_data['date'],
            hour = validated_data['hour'],
            content = validated_data['content'],
            language = validated_data['language'],
            formation = validated_data['formation'],
            category = validated_data['category'], 
            duration = validated_data['duration'],
            modality = validated_data['modality'],                
            inscription_start = validated_data['inscription_start'],
            inscription_end = validated_data['inscription_end'],
            requirements = validated_data['requirements'],         
            cost = validated_data['cost'],
            price = validated_data['price'],
            observations = validated_data['observations'],
            cultural_space = cultural_space
        )
        course.save()
        return course
    
    def update(self, instance, validated_data): 
        instance.image = validated_data.get('image', instance.image)
        instance.name = validated_data.get('name', instance.name)
        instance.teacher = validated_data.get('teacher', instance.teacher)
        instance.description = validated_data.get('description', instance.description)
        instance.date = validated_data.get('date', instance.date)
        instance.hour = validated_data.get('hour',instance.hour)
        instance.content = validated_data.get('content', instance.content)
        instance.language = validated_data.get('language', instance.language)
        instance.formation = validated_data.get('formation', instance.formation)
        instance.category = validated_data.get('category',instance.category) 
        instance.duration = validated_data.get('duration', instance.duration)
        instance.modality = validated_data.get('modality', instance.modality)
        instance.inscription_start = validated_data.get('inscription_start', instance.inscription_start)
        instance.inscription_end = validated_data.get('inscription_end', instance.inscription_end)
        instance.requirements = validated_data.get('requirements', instance.requirements)         
        instance.cost = validated_data.get('cost', instance.cost)
        instance.price = validated_data.get('price', instance.price)
        instance.observations = validated_data.get('observations',instance.observations)
        instance.cultural_space = validated_data.get('cultural_space',instance.cultural_space)
        instance.save()
        return instance
    
    def to_representation(self,instance):
       from culturalspaces.serializers import CulturalSpaceSerializer
       return {
             'id': instance.id,
            'image': instance.image.url,
             'name': instance.name,
             'teacher': instance.teacher,
             'description': instance.description,
             'date': instance.date,
             'hour': instance.hour,
             'content': instance.content,
             'language': instance.language,
             'formation': instance.formation,
             'category': instance.category,
             'duration': instance.duration,
             'modality': instance.modality,
             'inscription_start': instance.inscription_start,
             'inscription_end': instance.inscription_end,
             'requirements': instance.requirements,
             'cost': instance.cost,
             'price': instance.price,
             'observations': instance.observations,
             'cultural_space': CulturalSpaceSerializer(instance.cultural_space).data,
        }  