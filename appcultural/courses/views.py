from courses.serializers import CourseSerializer
from courses.models import Course
from rest_framework import status, generics
from rest_framework import permissions
from rest_framework.response import Response
from users.models import Coordinador
from culturalspaces.models import CulturalSpace
from django.forms import model_to_dict
from rest_framework.views import APIView
import datetime
import os
class AddCourseView(generics.CreateAPIView):
    serializer_class = CourseSerializer
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        """
        Add a new course

        Add course
        """
        if request.user.is_authenticated:
            coordinador = Coordinador.objects.filter(user_id=request.user.id).first()
            cultural_space = CulturalSpace.objects.filter(id = coordinador.cultural_space.id).first()
            if cultural_space:
                course_serializer = CourseSerializer(data=request.data, context = {"request": cultural_space})
                if course_serializer.is_valid():
                    course_serializer.save()
                    return Response(course_serializer.data, status = status.HTTP_201_CREATED)
                return Response(course_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
            return Response('message: El espacio cultural no es válido', status=status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status=status.HTTP_400_BAD_REQUEST)

class UpdateCourseView(generics.UpdateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = CourseSerializer

    def get_queryset(self,pk):
        return self.get_serializer().Meta.model.objects.filter(id=pk).first()

    def patch(self, request, pk=None):
        """
        Update (patch) a course by id

        Update course
        """
        if request.user.is_authenticated:
            course = Course.objects.filter(id=pk).first()
            if course:
                course_serializer = CourseSerializer(course, data=request.data)
                if course_serializer.is_valid():
                    course_serializer.save()
                    return Response(course_serializer.data, status = status.HTTP_200_OK)
                return Response(course_serializer.errors,status = status.HTTP_400_BAD_REQUEST)
            return Response('message: No existe un taller con estos datos',status = status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status = status.HTTP_400_BAD_REQUEST)

        # production = self.get_queryset(pk)
        # if production:
        #     production_serializer = self.serializer_class(production)  
        #     return Response(production_serializer.data, status=status.HTTP_201_CREATED)
        # return Response(status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk=None):
        """
        Update (replace) a course by id

        Update course
        """
        if self.get_queryset(pk):
            course_serializer = self.serializer_class(self.get_queryset(pk), data = request.data)
            if course_serializer.is_valid():
                course_serializer.save()
                return Response(course_serializer.data, status=status.HTTP_201_CREATED)
            return Response(course_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListCourseView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = CourseSerializer

    def get(self, request):
        """
        Get all courses 

        Return a list of all courses 
        """
        if request.user.is_authenticated:
            courses = Course.objects.order_by('name').all()
            course_serializer = CourseSerializer(courses, many=True)
            return Response(course_serializer.data, status=status.HTTP_200_OK)
        return Response('message: El usuario no está autenticado', status=status.HTTP_400_BAD_REQUEST)

class ListCoursesView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = CourseSerializer

    def get(self, request):
        """
        Get all courses by my cultural space

        Return a list of all courses by my cultural space
        """
        if request.user.is_authenticated:
            coordinador = Coordinador.objects.filter(user_id=request.user.id).first()
            if coordinador.cultural_space != None:
                courses = Course.objects.filter(cultural_space_id = coordinador.cultural_space.id).order_by('name').all()
                courses_serializer = CourseSerializer(courses, many=True, context={'request': request})
                return Response(courses_serializer.data, status=status.HTTP_200_OK)
        return Response('message: El usuario no está autenticado', status=status.HTTP_400_BAD_REQUEST)

class DeleteCourseView(generics.DestroyAPIView):
    serializer_class = CourseSerializer
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, pk=None):
        """
        Delete a course by id
        
        Delete course
        """
        if request.user.is_authenticated:
            course = Course.objects.filter(id=pk).first()
            if course:
                course.delete()
                return Response({'message: Taller eliminado correctamente'}, status = status.HTTP_200_OK)
            return Response({'message: No se ha encontrado un taller con estos datos'}, status = status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status = status.HTTP_200_OK)

class GetCoursesAgendaView(APIView):
    permission_classes = (permissions.AllowAny,)
   
    def get(self, request):
        """
        Get all culturalspaces, courses and coordinador phone

        Get all courses
        """
        start_calendar = datetime.datetime.today()
        result = []
        coordinadores = Coordinador.objects.select_related('cultural_space').all()
        for c in coordinadores:
            if c.cultural_space != None:
                user = model_to_dict(c.user, fields=['name','phone'])
                
                courses = Course.objects.filter(cultural_space_id = c.cultural_space.id).all()
               
                for p in courses:
                    course = model_to_dict(p, fields=['id','name','image','description','content', 'requirements','teacher','date','hour','category','language', 'modality','cost','price','observations','cultural_space'])
                    course['image'] = course['image'].url
                    cultural_space =  model_to_dict(p.cultural_space, fields=['id','image','name', 'address', 'email','phone','localidad','partido', 'latitude','longitude', 'facebook', 'instagram'])
                    cultural_space['image'] = cultural_space['image'].url
                    result_response = {"coordinador":user, "cultural_space": cultural_space, "course": course }
                
                    result.append(result_response)
        return Response(result, status=status.HTTP_200_OK)

class GetCourseAgendaView(APIView):
    permission_classes = (permissions.AllowAny,)
   
    def get(self, request, pk):
        """
        Get all productions and culturalspaces

        Get course
        """

        result = []

        courses = Course.objects.filter(id=pk).all()
        for p in courses:
            course = model_to_dict(p, fields=['id','name','image','description','content', 'requirements','teacher' , 'date', 'hour', 'inscription_start', 'inscription_end','category','language', 'modality','cost','price','observations','cultural_space'])
            course['image'] = course['image'].url
            cultural_space =  model_to_dict(p.cultural_space, fields=['id','image','name', 'address', 'email','phone','localidad','partido', 'latitude','longitude', 'facebook', 'instagram'])
            cultural_space['image'] = cultural_space['image'].url

            coordinador = Coordinador.objects.filter(cultural_space_id = p.cultural_space).all()
            for c in coordinador:
                if c.cultural_space != None:
                    user = model_to_dict(c.user, fields=['name','phone'])
       
                    result_response = {"coordinador":user, "cultural_space": cultural_space, "course": course }
                    result.append(result_response)
        return Response(result, status=status.HTTP_200_OK)


class GetCoursesByCulturalSpaceView(APIView):
   permission_classes = (permissions.AllowAny,)
    
   def get(self, request, pk=None):        
        """
        Get all courses (formation) by cultural space

        Get all courses by cultural space id
        """
        result = []
        coordinadores = Coordinador.objects.filter(cultural_space = pk).all()
        for c in coordinadores:
             if c.cultural_space != None:
                 user = model_to_dict(c.user, fields=['name','phone'])
                
                 courses = Course.objects.filter(cultural_space_id = c.cultural_space.id).all()
               
                 for p in courses:
                    course = model_to_dict(p, fields=['id','name','image','content', 'description','requirements' 'teacher','date','hour','category','language', 'modality','cost','price','observations','cultural_space'])
                    course['image'] = course['image'].url
                    cultural_space =  model_to_dict(p.cultural_space, fields=['id','image','name', 'address', 'email','phone','localidad','partido', 'latitude','longitude', 'facebook', 'instagram'])
                    cultural_space['image'] = cultural_space['image'].url
                    result_response = {"coordinador":user, "cultural_space": cultural_space, "course": course }
                
                    result.append(result_response)
        return Response(result, status=status.HTTP_200_OK)