# Generated by Django 3.1.7 on 2022-05-22 17:45

import courses.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to=courses.models.upload_path)),
                ('name', models.CharField(default='', max_length=100)),
                ('description', models.TextField()),
                ('date', models.CharField(default='', max_length=100)),
                ('hour', models.CharField(default='', max_length=100)),
                ('inscription_start', models.DateField(default='')),
                ('inscripcion_end', models.DateField(default='')),
                ('teacher', models.CharField(default='', max_length=100)),
                ('category', models.IntegerField(choices=[(1, 'Adultos'), (2, 'Público en general'), (3, 'Niños')])),
                ('content', models.TextField(default='', max_length=250)),
                ('modality', models.IntegerField(choices=[(1, 'Presencial'), (2, 'Virtual')])),
                ('language', models.IntegerField(choices=[(1, 'Audiovisuales'), (2, 'Artes Visuales'), (3, 'Artesanías'), (4, 'Comunicación'), (5, 'Danza'), (6, 'Letras'), (7, 'Música'), (8, 'Teatro')])),
                ('cost', models.IntegerField(choices=[(1, 'A la gorra'), (2, 'Con precio'), (3, 'Gratuito')])),
                ('price', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('requirements', models.TextField()),
                ('duration', models.IntegerField(choices=[(1, 'Anual'), (2, 'Mensual'), (3, 'Semestral')])),
                ('formation', models.IntegerField(choices=[(1, 'Curso'), (2, 'Congreso'), (3, 'Diplomatura'), (4, 'Encuentro'), (5, 'Jornada'), (6, 'Seminario'), (7, 'Workshop')])),
                ('observations', models.TextField()),
            ],
            options={
                'verbose_name': 'Course',
                'verbose_name_plural': 'Courses',
            },
        ),
    ]
