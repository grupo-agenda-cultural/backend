# Generated by Django 3.1.7 on 2022-07-06 17:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0009_auto_20220704_1509'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='observations',
            field=models.TextField(blank=True, default='', null=True),
        ),
    ]
