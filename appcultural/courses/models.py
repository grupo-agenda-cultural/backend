from django.db import models
from .configuraciones import CHOICE_CATEGORIES, CHOICE_COSTS, CHOICE_DURATIONS, CHOICE_FORMATIONS, CHOICE_MODALITIES, CHOICE_LANGUAGES


def upload_path(instance, filename):
    return '/'.join(['courses',str(instance.name),filename])

class Course(models.Model):
    image =  models.ImageField(upload_to=upload_path, null=True, blank=True)
    name = models.CharField(max_length=100, blank=False, default='')
    teacher = models.CharField(max_length=100, blank=False, default='') # lista de docentes
    description = models.TextField()
    date = models.CharField(max_length=100, blank=False, default='')
    hour = models.CharField(max_length=100, blank=False, default='')
    content = models.TextField(blank=False, default='')
    language = models.IntegerField(blank=False, choices = CHOICE_LANGUAGES)
    formation =  models.IntegerField( blank=False,choices = CHOICE_FORMATIONS)
    category = models.IntegerField( blank=False, choices = CHOICE_CATEGORIES)
    duration = models.IntegerField( blank=False, choices = CHOICE_DURATIONS)
    modality= models.IntegerField(blank=False, choices = CHOICE_MODALITIES)
    inscription_start = models.DateField(blank=False, default='')
    inscription_end = models.DateField(blank=False, default='')
    requirements = models.TextField(blank=True, null=True, default='') 
    cost = models.IntegerField(blank=False, choices = CHOICE_COSTS)
    price = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    observations = models.TextField(blank=True, null=True, default='')
    # Observaciones. En el campo Observaciones, se tiene que aclarar si en el Taller hay días en los que s
    cultural_space = models.ForeignKey("culturalspaces.CulturalSpace", on_delete=models.CASCADE, default="", blank=False, null=True, related_name='cultural_space_course')

    class Meta:
        verbose_name = 'Course'
        verbose_name_plural = 'Courses'

    def __str__(self):
        return self.name
    