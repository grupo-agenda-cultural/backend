

from django.urls import path, re_path
from .views import AddCourseView, UpdateCourseView, DeleteCourseView, ListCoursesView, GetCoursesAgendaView,GetCourseAgendaView, ListCourseView, GetCoursesByCulturalSpaceView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('courses/', ListCourseView.as_view(), name='list_courses_view'),
    path('courses/my-cultural-space/', ListCoursesView.as_view(), name='list_courses_view'),
    path('courses/add/', AddCourseView.as_view(), name='add_course_view'),
    path('courses/update/<int:pk>/', UpdateCourseView.as_view(), name='update_course_view'),
    path('courses/delete/<int:pk>/', DeleteCourseView.as_view(), name='delete_course_view'),
    
    path('courses/formation/', GetCoursesAgendaView.as_view(), name='get_course_view'),
    path('courses/formation/<int:pk>/', GetCourseAgendaView.as_view(), name='get_course_view'),
    path('courses/formation/culturalspaces/<int:pk>/',GetCoursesByCulturalSpaceView.as_view(), name='get_courses_cultural_space')
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
