from django.apps import AppConfig


class CulturalspacesConfig(AppConfig):
    name = 'culturalspaces'
