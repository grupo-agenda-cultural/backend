from django.db import models
from productions.models import Production
from .configuraciones import CHOICE_PARTIDOS, CHOICE_LOCALIDADES


def upload_path(instance, filename):
    return '/'.join(['culturalspaces', str(instance.name), filename])

class CulturalSpace(models.Model):
    image = models.ImageField(upload_to=upload_path, null=True, blank=True)
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    phone = models.CharField(null=True, max_length=20)
    email = models.CharField(max_length=255, unique=True)
    partido = models.IntegerField(blank=False, choices=CHOICE_PARTIDOS)
    localidad = models.IntegerField(blank=False, choices=CHOICE_LOCALIDADES)
    latitude = models.FloatField()
    longitude = models.FloatField()
    facebook = models.CharField(null=True, blank=True, max_length=255)
    instagram = models.CharField(null=True, blank=True, max_length=255)

    class Meta:
        verbose_name = 'Espacio Cultural'
        verbose_name_plural = 'Espacios culturales'
        ordering = ['name']


    def __str__(self):
        return f'{self.id} {self.name}'
