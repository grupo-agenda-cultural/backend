from django.contrib import admin
from .models import CulturalSpace

class SettingCulturalSpace(admin.ModelAdmin):
    list_display = ('name', 'address', 'email','phone')

# Register your models here.
admin.site.register(CulturalSpace, SettingCulturalSpace)