from django.urls import path, re_path
from .views import AddCulturalSpaceView, ListCulturalSpaceView, UpdateCulturalSpaceView, GetCulturalSpaceView

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('culturalspaces/', ListCulturalSpaceView.as_view(), name='list_cultural_spaces'),
    path('culturalspaces/add/', AddCulturalSpaceView.as_view(), name="register_culturalSpace"),
    path('culturalspaces/update/<int:pk>/', UpdateCulturalSpaceView.as_view(), name="update_cultural_space"),
    path('culturalspaces/<int:pk>/', GetCulturalSpaceView.as_view(), name="get_cultural_space"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

