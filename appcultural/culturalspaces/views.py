from functools import partial
from venv import create
from rest_framework.views import APIView
from productions.serializers import ProductionSerializer
from .models import CulturalSpace
from rest_framework import status, permissions, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import CulturalSpaceSerializer
from users.models import Coordinador
from productions.models import Production
from django.db.models import Prefetch
from django.forms import model_to_dict
from django.http import JsonResponse
from django.core import serializers
from users.serializers import CoordinadorSerializer
from django_filters.rest_framework import DjangoFilterBackend
import datetime
class AddCulturalSpaceView(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated,]
    serializer_class = CulturalSpaceSerializer

    def post(self, request):
        """
        Add a cultural space
        
        Add a new cultural space 
        """
        if request.user.is_authenticated:
            culturalSpace_serializer = CulturalSpaceSerializer(data=request.data)
            if culturalSpace_serializer.is_valid():
                culturalSpace_serializer.save()
                coordinador = Coordinador.objects.filter(user_id = request.user.id).first()
                coordinador.cultural_space = CulturalSpace.objects.filter(id = culturalSpace_serializer.data['id']).first()
                if coordinador:
                    coordinador_serializer = CoordinadorSerializer(coordinador, data=request.data, partial=True)                   
                    if coordinador_serializer.is_valid():
                        coordinador_serializer.save()
                        return Response({'message: Espacio Cultural creado correctamente'}, status=status.HTTP_201_CREATED)
                    return Response({'message: El coordinador no es válido'}, status=status.HTTP_201_CREATED)
                return Response({'message: No se agregó el espacio cultural correctamente'}, status=status.HTTP_400_BAD_REQUEST)
            return Response(culturalSpace_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status=status.HTTP_400_BAD_REQUEST)

class ListCulturalSpaceView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CulturalSpaceSerializer

    def get(self, request):
        """
        Get all cultural spaces
        
        Return a list of all cultural spaces 
        """
        culturalSpaces = CulturalSpace.objects.order_by('name').all()
        culturalSpace_serializer = CulturalSpaceSerializer(
            culturalSpaces, many=True, context = {'request': request})
        return Response(culturalSpace_serializer.data, status=status.HTTP_200_OK)

class GetCulturalSpaceView(generics.RetrieveAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CulturalSpaceSerializer

    def get(self, request, pk=None):
        """
        Get cultural space by id
        
        Get cultural space
        """
        culturalSpace = CulturalSpace.objects.filter(id=pk).first() 
        if culturalSpace:
            culturalSpace_serializer = CulturalSpaceSerializer(culturalSpace)
            return Response(culturalSpace_serializer.data, status=status.HTTP_200_OK)
        return Response({'message: No se ha encontrado un espacio cultural con estos datos'}, status=status.HTTP_400_BAD_REQUEST)

class ListCoordinatorCulturalSpaceView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request):
        culturalSpaces = CulturalSpace.objects.all()
        culturalSpace_serializer = CulturalSpaceSerializer(
            culturalSpaces, many=True)
        return Response(culturalSpace_serializer.data, status=status.HTTP_200_OK)

class UpdateCulturalSpaceView(generics.UpdateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = CulturalSpaceSerializer

    def perform_update(self, serializer):

        return super().perform_update(serializer)

    def get_queryset(self,pk):
       # return self.get_serializer().Meta.model.objects.filter(
        #  CulturalSpace=self.kwargs['pk']
        #)
         return self.get_serializer().Meta.model.objects.filter(id=pk).first()
       # return self.get_serializer().Meta.model.objects.filter(state=True).filter(id=pk).first()


    def patch(self, request, pk=None):
        """
        Update (patch) a cultural space by id

        Update a cultural space 
        """
        if request.user.is_authenticated:
            cultural_space = CulturalSpace.objects.filter(id=pk).first()
            if cultural_space:
                culturalSpace_serializer = CulturalSpaceSerializer(cultural_space, data=request.data)
                if culturalSpace_serializer.is_valid():
                    culturalSpace_serializer.save() 
                    return Response(culturalSpace_serializer.data, status=status.HTTP_200_OK)
                #return Response('message: El espacio cultural no es válido', status=status.HTTP_400_BAD_REQUEST)
            return Response('message: No existe un espacio cultural con estos datos',status=status.HTTP_400_BAD_REQUEST)
        return Response('message: El usuario no está autenticado', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk=None):
        """
        Update (replace) a cultural space by id

        Update a cultural space 
        """
        if self.get_queryset(pk):
            culturalSpace_serializer = self.serializer_class(self.get_queryset(pk), data=request.data)
            if culturalSpace_serializer.is_valid():
                culturalSpace_serializer.save()
                return Response(culturalSpace_serializer.data, status=status.HTTP_201_CREATED)
            return Response(culturalSpace_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


