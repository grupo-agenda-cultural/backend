
CHOICE_PARTIDOS = (
    (1, 'José C. Paz'),
    (2, 'Malvinas Argentinas'),
    (3, 'San Miguel')
)

CHOICE_LOCALIDADES = [
    (1,'Área de Promoción El Triángulo'),
    (2,'Bella Vista'),
    (3,'Campo de Mayo'),
    (4,'Grand Bourg'),
    (5,'Ingeniero Adolfo Sourdeaux'),
    (6,'Ingeniero Pablo Nogués'),
    (7,'José C. Paz'),
    (8,'Los Polvorines'),
    (9,'Muñiz'),
    (10,'San Miguel'),
    (11,'Santa María'),
    (12,'Tierras Altas'),    
    (13,'Tortuguitas'),
    (14,'Villa de Mayo')
]
