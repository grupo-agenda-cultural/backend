from rest_framework import serializers
from .models import CulturalSpace
from productions.serializers import ProductionSerializer

class CulturalSpaceSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    image = serializers.ImageField(use_url=True, allow_null=True, required=False)
    name = serializers.CharField(max_length=255)
    address = serializers.CharField(max_length=255)
    phone = serializers.CharField(max_length=20)
    email = serializers.CharField(max_length=255)
    partido = serializers.CharField()
    localidad = serializers.CharField()
    latitude = serializers.FloatField()
    longitude = serializers.FloatField()
    facebook = serializers.CharField(allow_blank=True)
    instagram = serializers.CharField(allow_blank=True)
    class Meta:
        model = CulturalSpace
        fields = '__all__'
        
    def create(self, validated_data):
        return CulturalSpace.objects.create(**validated_data)

    def update(self, instance, validated_data): 
        instance.name = validated_data.get('name', instance.name)
        instance.image = validated_data.get('image', instance.image)
        instance.address = validated_data.get('address', instance.address)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.email = validated_data.get('email', instance.email)
        instance.partido = validated_data.get('partido', instance.partido)
        instance.localidad = validated_data.get('localidad', instance.localidad)
        instance.latitude = validated_data.get('latitude', instance.latitude)
        instance.longitude = validated_data.get('longitude', instance.longitude)
        instance.facebook = validated_data.get('facebook', instance.facebook)
        instance.instagram = validated_data.get('instagram', instance.instagram)
        instance.save()
        return instance

    def to_representation(self,instance):
       return {
             'id': instance.id,
             'name': instance.name,
             'image':instance.image.url,
             'address': instance.address,
             'phone': instance.phone,
             'email': instance.email,
             'partido': instance.partido,
            'localidad': instance.localidad,
             'latitude': instance.latitude,
             'longitude': instance.longitude,
             'facebook': instance.facebook,
            'instagram': instance.instagram,
        }  